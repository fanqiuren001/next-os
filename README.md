# NextOS

NextOS 是nextVPU公司基于openwrt操作系统的基础上推出适合在nextVPU公司推出的视觉芯片的操作系统，为应用层提供统一开发、编译、部署和运行环境，降低基于linux嵌入式智能硬件的开发难度，缩短开发时间。NextOS 基于openwrt的强大的自动构建能力、多芯片适配能力、强大的扩展能力的基础上，搭建应用开发Framework， 提供多语言开发环境。底层和原生框架层基于C 和C++构建， 提供两种脚本语言的应用层框架。资源比较受限的嵌入式系统，推荐用lua语言做应用层开发。资源比较丰富的嵌入式系统，推荐用JavaScript语言进行开发。



## 一、系统框架图



## 二、核心组件简介



## 三、编程说明







​	http://www.openwrt.org

